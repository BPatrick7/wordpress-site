# LAMP

Creating and customizing a Wordpress site

## Requirements

  - Completing the [LAMP Stack](https://gitlab.com/coditech/exercises/LAMP) exercise

## Goals

  - Get acquainted with Wordpress
  - **Competencies**:
    - <kbd>Managing a CMS</kbd> 
    - <kbd>Using a CMS</kbd>

## Notes

Certain Linux distributions will propose to install Wordpress through the package manager. You **do not** want to do that. This works when you want to make a server that only serves one wordpress installation. This is not your case, you need more flexibility.  
Thus, make sure that you're downloading the `wordpress.tar.gz` from the official site and following official installation procedure

- Choose a goal for your website; Whatever your website is, it should feature at least:
  - a gallery
  - a contact form
  - one or more static pages ("about us")
  - a customized theme
  - a logo
- feel free to copy something that exists


## Tasks

- Install Wordpress 
- Install a Theme and customize it <kbd>🔑🔑</kbd>
- Create a contact form, make sure it works <kbd>🔑</kbd>
- Create a gallery, make sure it works <kbd>🔑</kbd>
- Do any additional customization that are needed <kbd>🔑x1 ~ 🔑x30</kbd>